let movies = [
	m1={
		title: "The Lord of the Rings Trilogy",
		genre: "action",
		releasedDate: Date(2001,12,10),
		rating:4.8
	},
	m2={
		title: "The Shawshank Redemption",
		genre: "drama",
		releasedDate: new Date(1994,9,10),
		rating:4.9
	},
	m3={
		title: "Life is Beautiful",
		genre: "drama",
		releasedDate: new Date(1997,12,20),
		rating:4.9
	},
	m4={
		title: "Jesus Christ Superstar",
		genre: "musical",
		releasedDate: new Date(1973,6,26),
		rating:4.5
	},
	m5={
		title: "Bruce Almighty", //
		genre: "comedy",
		releasedDate: new Date(2003,5,14),
		rating:4.7
	},
	m6={
		//function that displays 5 movies
		displayRating: function(){
			for(let i = 0; i < (movies.length-2); i++){
				console.log("The movie " + movies[i].title + " has " + movies[i].rating + " stars.")
			}
		}
	},
	m7={
		//function that asks for movie title
		displayRating2: function(movieTitle){
			let i = 0;
			do{
				if(movies[i].title==movieTitle){
					console.log("The movie " + movies[i].title + " has " + movies[i].rating + " stars.")
				}
				i++;
			}
			while(i < movies.length-2)
		}
	}
	]

let vowels = ["a","e","i","o","u"];

let i = 0;
function showAllMovies(){
	while (i < movies.length-2){
		if (vowels.indexOf(movies[i].genre[0])==-1){
		console.log(movies[i].title + " is a " + movies[i].genre + " movie.");
		}
		else{
		console.log(movies[i].title + " is an " + movies[i].genre + " movie.");
		}
		i++;
	}
}

movies[5].displayRating(); //displays 5 movies
console.log("-----")
showAllMovies(); // AN action movie | A comedy movie
console.log("-----")
console.log("Bonus: Type rateMovie() and use a movie as an argument (e.g. 'Bruce Almighty')");

function rateMovie(movie){
	movies[6].displayRating2(movie);
}

// rateMovie("Bruce Almighty");

// m1:{title://string
// 	genre://string
// 	releasedDate://date
// 	rating://number}